#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    // variables
    Player(Side side);
    ~Player();
    Side us, them;
    std::vector<Move*> allmoves;
    std::vector<Move*> moves;
    int valuetable[64] = { 
	10, 3, 7, 7, 7, 7, 3, 10,
	 3, 1, 5, 5, 5, 5, 1,  3,
	 7, 5, 5, 5, 5, 5, 5,  7,
  	 7, 5, 5, 5, 5, 5, 5,  7,
  	 7, 5, 5, 5, 5, 5, 5,  7,
  	 7, 5, 5, 5, 5, 5, 5,  7,
  	 3, 1, 5, 5, 5, 5, 1,  3,
    	10, 3, 7, 7, 7, 7, 3, 10
    };

    // functions
    Move* doMove(Move *opponentsMove, int msLeft);
    std::vector<Move*> listmoves(Board *board, Side side);
    Move* simpleselect(std::vector<Move*> moves);
    Move* deepselect(Board *board, std::vector<Move*> moves, Side side, int depth);


    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board *othello;

};

#endif


#include "player.h"
#include <new>
#include <unistd.h>

/*
 * This is a simple heruistic that basically counts whether or not
 * you have more chips at the end
 */
 
Move* Player::simpleselect(std::vector<Move*> moves){
    if(moves.size() < 1){
        return NULL;
    }
    Move* bestmove = NULL;
    int bestscore = -64;
    int tempscore;
    for(std::vector<Move*>::iterator it = moves.begin(); it != moves.end(); ++it){
        Board *tempboard = othello->copy();
        tempboard->doMove(*it, us);
        tempscore = tempboard->count(us)-tempboard->count(them);
        int ind = (*it)->getX() + 8 * (*it)->getY();
        //std::cerr<<ind <<std::endl;
        tempscore += valuetable[ind];
        if(tempscore >= bestscore){
            bestscore=tempscore;
            bestmove = *it;
        }
    }
    Move* returnmove = new Move(bestmove->getX(),bestmove->getY());
    for(std::vector<Move*>::iterator it = moves.begin(); it != moves.end(); ++it){
        delete *it;
    }
    
    
    return returnmove;
}

Move* Player::deepselect(Board *board, std::vector<Move*> moves, Side side, int depth){
    if(moves.size() < 1){
        return NULL;
    }
    Move* bestmove = NULL;
    int bestscore = -64;
    int tempscore;
    Side oside = (side == BLACK ? WHITE : BLACK);
    for(std::vector<Move*>::iterator it = moves.begin(); it != moves.end(); ++it){
        Board *tempboard = board->copy();
        tempboard->doMove(*it, side);
        //this is where it gets tricky
        if(depth > 1){
            //std::cerr << "depth 2" << std::endl;
            std::vector<Move*> omoves = listmoves(tempboard, oside);
            Move* omove = deepselect(tempboard, omoves, oside, depth-1);
            if(omove != NULL){
            //std::cerr << omove->getX() << " " << omove->getY() << std::endl;
            }
            tempboard->doMove(omove, oside);
        }
        tempscore = tempboard->count(side)-tempboard->count(oside);
        //std::cerr << tempscore << std::endl;
        int ind = (*it)->getX() + 8 * (*it)->getY();
        //std::cerr<<ind <<std::endl;
        if(!testingMinimax){
            tempscore += valuetable[ind];
        }
        if(tempscore >= bestscore){
            bestscore=tempscore;
            bestmove = *it;
        }
    }
    Move* returnmove = new Move(bestmove->getX(),bestmove->getY());
    for(std::vector<Move*>::iterator it = moves.begin(); it != moves.end(); ++it){
        delete *it;
    }

    return returnmove;
}
std::vector<Move*> Player::listmoves(Board *board, Side side) {
    vector<Move*> list;
    for(std::vector<Move*>::iterator it = allmoves.begin(); it != allmoves.end(); ++it){
        //std::cerr << (*it)->getX() << " " << (*it)->getY() << std::endl;
        if (board->checkMove(*it, side)){
            int x = (*it)->getX();
            int y = (*it)->getY();
            //std::cerr << x << " " << y << std::endl;
            list.push_back(new Move(x,y));
            //std::cerr << "move here!" <<std::endl;
        }
    }
    return list;
}


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.d
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    othello = new Board();
    us = side;
    if(us == BLACK){
        them = WHITE;
    }
    else {
        them = BLACK;
    }

    for(int x = 0; x < 8; x++){
        for(int y = 0; y < 8; y++){
            allmoves.push_back(new Move(x,y));
        }
    }
    std::cerr << "player setup complete" << endl;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    for (std::vector<Move*>::iterator it = allmoves.begin(); it != allmoves.end(); ++it){
        delete (*it);
    }
    delete othello;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    othello->doMove(opponentsMove, them);
    //std::cerr << "Moves" << std::endl;
    moves = listmoves(othello, us);
    //std::cerr << "Scores" << std::endl;
    //othello.
    if(moves.size() < 1){
        return NULL;
    }
    else {
        Move* bestmove = deepselect(othello, moves, us, 5);
        othello->doMove(bestmove,us);
        return bestmove;
    }
}

// TEST for git commit - James
